package org.carlospinsa.bean;
public class Alumno {
    private int clave;
    private String carne;
    private String apellido;
    private String nombre;
    public Alumno(){
    }
    public Alumno(int clave, String carne, String apellido, String nombre) {
        this.clave = clave;
        this.carne = carne;
        this.apellido = apellido;
        this.nombre = nombre;
    }
    public int getClave() {
        return clave;
    }
    public void setClave(int clave) {
        this.clave = clave;
    }
    public String getCarne() {
        return carne;
    }
    public void setCarne(String carne) {
        this.carne = carne;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
    
