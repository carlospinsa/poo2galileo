package org.carlospinsa.bean;
public class Clase {
private int codigoClase;
private String descripcion;
    public Clase() {
    }
    public Clase(int codigoClase, String descripcion) {
        this.codigoClase = codigoClase;
        this.descripcion = descripcion;
    }
    public int getCodigoClase() {
        return codigoClase;
    }
    public void setCodigoClase(int codigoClase) {
        this.codigoClase = codigoClase;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }    
}
