package org.carlospinsa.bean;
import org.carlospinsa.bean.Clase;
import org.carlospinsa.bean.Estado;
public class Nota {
private int codigoNota;
private int clave;
private Clase descripcion;
private double primeraUnidad;
private double segundaUnidad;
private double terceraUnidad;
private double cuartaUnidad;
private double quintaUnidad;
private Estado estado;
    public Nota() {
    }
    public Nota(int codigoNota, int clave, Clase descripcion, double primeraUnidad, double segundaUnidad, double terceraUnidad, double cuartaUnidad, double quintaUnidad, Estado estado) {
        this.codigoNota = codigoNota;
        this.clave = clave;
        this.descripcion = descripcion;
        this.primeraUnidad = primeraUnidad;
        this.segundaUnidad = segundaUnidad;
        this.terceraUnidad = terceraUnidad;
        this.cuartaUnidad = cuartaUnidad;
        this.quintaUnidad = quintaUnidad;
        this.estado = estado;
    }
      public Nota(int codigoNota, int clave, double primeraUnidad, double segundaUnidad, double terceraUnidad, double cuartaUnidad, double quintaUnidad) {
        this.codigoNota = codigoNota;
        this.clave = clave;
        this.primeraUnidad = primeraUnidad;
        this.segundaUnidad = segundaUnidad;
        this.terceraUnidad = terceraUnidad;
        this.cuartaUnidad = cuartaUnidad;
        this.quintaUnidad = quintaUnidad;
    }
    public int getCodigoNota() {
        return codigoNota;
    }
    public void setCodigoNota(int codigoNota) {
        this.codigoNota = codigoNota;
    }
    public int getClave() {
        return clave;
    }
    public void setClave(int clave) {
        this.clave = clave;
    }
    public Clase getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(Clase descripcion) {
        this.descripcion = descripcion;
    }
    public double getPrimeraUnidad() {
        return primeraUnidad;
    }
    public void setPrimeraUnidad(double primeraUnidad) {
        this.primeraUnidad = primeraUnidad;
    }
    public double getSegundaUnidad() {
        return segundaUnidad;
    }
    public void setSegundaUnidad(double segundaUnidad) {
        this.segundaUnidad = segundaUnidad;
    }
    public double getTerceraUnidad() {
        return terceraUnidad;
    }
    public void setTerceraUnidad(double terceraUnidad) {
        this.terceraUnidad = terceraUnidad;
    }
    public double getCuartaUnidad() {
        return cuartaUnidad;
    }
    public void setCuartaUnidad(double cuartaUnidad) {
        this.cuartaUnidad = cuartaUnidad;
    }
    public double getQuintaUnidad() {
        return quintaUnidad;
    }
    public void setQuintaUnidad(double quintaUnidad) {
        this.quintaUnidad = quintaUnidad;
    }
    public Estado getIdEstado() {
        return estado;
    }
    public void setEstado(Estado estado) {
        this.estado = estado;
    }   
}
