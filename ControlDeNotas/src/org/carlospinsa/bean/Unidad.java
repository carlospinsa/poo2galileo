package org.carlospinsa.bean;
public class Unidad {
    private int idUnidad;
    private String unidad;
    public Unidad() {
    }
    public Unidad(int idUnidad, String unidad) {
        this.idUnidad = idUnidad;
        this.unidad = unidad;
    }
    public int getIdUnidad() {
        return idUnidad;
    }
    public void setIdUnidad(int idUnidad) {
        this.idUnidad = idUnidad;
    }
    public String getUnidad() {
        return unidad;
    }
    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }    
}
