package org.carlospinsa.db;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.sql.Statement;
import com.microsoft.sqlserver.jdbc.SQLServerDriver;
public class Conexion {
    private Connection conexion;
    private Statement sentencia;
    private static Conexion instancia;
    public static Conexion getInstancia(){
        if(instancia == null){
            instancia = new Conexion();
        }
        return instancia;
    }
    public Conexion() {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            conexion = DriverManager.getConnection("jdbc:sqlserver://CARLOSPINSA-PC:0;"
                    + "instanceName=SQLEXPRESS;dataBaseName=ControlDeNotas;user=Profesor;"
                    + "password=Profesor;");
            sentencia = conexion.createStatement();
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }catch(InstantiationException e){
            e.printStackTrace();
        }catch(IllegalAccessException e){
            e.printStackTrace();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    public ResultSet hacerConsulta(String consulta){
        ResultSet resultado = null;
        try{
            resultado = sentencia.executeQuery(consulta);
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    public ResultSet ejecutarProcedimiento(String nombreProcedimiento, Map parametros, boolean respuesta){
        ResultSet resultado = null;
        try{
            CallableStatement procedimiento = conexion.prepareCall(nombreProcedimiento);
            Iterator iterador = parametros.entrySet().iterator();
            while(iterador.hasNext()){
                Map.Entry e = (Map.Entry)iterador.next();
                String clase = e.getValue().getClass().toString();
                System.out.println(clase);
                int longitud = clase.length();
                String tipoDato = clase.substring(clase.lastIndexOf(".") + 1,longitud);
                System.out.println(tipoDato);
                if(tipoDato.equals("String")){
                    procedimiento.setString(e.getKey().toString(),e.getValue().toString());
                }else if(tipoDato.equals("Integer")){
                    procedimiento.setInt(e.getKey().toString(),Integer.parseInt(e.getValue().toString()));
                }else if(tipoDato.equals("Double")){
                    procedimiento.setDouble(e.getKey().toString(),Double.parseDouble(e.getValue().toString()));
                }else if(tipoDato.equals("Boolean")){
                    procedimiento.setBoolean(e.getKey().toString(),Boolean.parseBoolean(e.getValue().toString()));
                }
            }
            if(respuesta == true){
                resultado = procedimiento.executeQuery();
            }else{
                procedimiento.execute();
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }   
 }
   
