package org.carlospinsa.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import org.carlospinsa.modelo.ModeloVentanaNota;
import org.carlospinsa.db.Conexion;
public class VentanaNota extends JFrame implements ActionListener{
    private JButton btnAgregar;
    private JButton btnEliminar;
    private JButton btnSalir;
    private JButton btnBuscar;
    private JTable tblAlumno;
    private JScrollPane scrAlumno;
    private ModeloVentanaNota modelo;
    public VentanaNota(){
    this.setTitle("Listado Alumnos");
    this.setSize(800,700);
    this.setLayout(null);
    btnAgregar= new JButton ("Alumno nuevo");
    btnAgregar.setBounds(10,10,120,75);
    btnAgregar.addActionListener(this);
    btnEliminar= new JButton ("Quitar Alumno");
    btnEliminar.setBounds(10,80,120,75);
    btnEliminar.addActionListener(this);
    btnBuscar= new JButton ("Buscar Alumno");
    btnBuscar.setBounds(10,160,120,75);
    btnBuscar.addActionListener(this);
    btnSalir = new JButton ("Salir");
    btnSalir.setBounds(10,320,120,75);
    btnSalir.addActionListener(this);
    tblAlumno = new JTable();
    modelo = new ModeloVentanaNota();
    tblAlumno.setModel(modelo);
    scrAlumno = new JScrollPane ();
    scrAlumno.setBounds(150,10,550,650);
    scrAlumno.setViewportView(tblAlumno);
    this.getContentPane().add(btnAgregar);
    this.getContentPane().add(btnEliminar);
    this.getContentPane().add(btnBuscar);
    this.getContentPane().add(btnSalir);
    this.getContentPane().add(scrAlumno);  
    this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    this.setVisible(true);
    }
    public JButton getBtnAgregar() {
        return btnAgregar;
    }
    public void setBtnAgregar(JButton btnAgregar) {
        this.btnAgregar = btnAgregar;
    }
    public JButton getBtnEliminar() {
        return btnEliminar;
    }
    public void setBtnEliminar(JButton btnEliminar) {
        this.btnEliminar = btnEliminar;
    }
    public JButton getBtnSalir() {
        return btnSalir;
    }
    public void setBtnSalir(JButton btnSalir) {
        this.btnSalir = btnSalir;
    }
    public JButton getBtnBuscar() {
        return btnBuscar;
    }
    public void setBtnBuscar(JButton btnBuscar) {
        this.btnBuscar = btnBuscar;
    }
    public JTable getTblAlumno() {
        return tblAlumno;
    }
    public void setTblAlumno(JTable tblAlumno) {
        this.tblAlumno = tblAlumno;
    }
    public JScrollPane getScrAlumno() {
        return scrAlumno;
    }
    public void setScrAlumno(JScrollPane scrAlumno) {
        this.scrAlumno = scrAlumno;
    }
    public ModeloVentanaNota getModelo() {
        return modelo;
    }
    public void setModelo(ModeloVentanaNota modelo) {
        this.modelo = modelo;
    }
    public void actionPerformed(ActionEvent e){
                if(e.getSource() == getBtnAgregar()){
        JFileChooser seleccionDeArchivos = new JFileChooser (".xls");
        int seleccion = seleccionDeArchivos.showOpenDialog(null);
            if(seleccion == JFileChooser.APPROVE_OPTION){
                javax.swing.JOptionPane.showMessageDialog(null, seleccionDeArchivos.getSelectedFile().toString());
            } 
        }
    
    }
}
