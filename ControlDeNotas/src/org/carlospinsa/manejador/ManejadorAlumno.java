package org.carlospinsa.manejador;
import java.util.ArrayList;
import org.carlospinsa.bean.Alumno;
public class ManejadorAlumno {
    private ArrayList<Alumno> listaDeAlumnos = new ArrayList<Alumno>();
    public ManejadorAlumno(){
    }
    public void agregar(Alumno elemento){
        listaDeAlumnos.add(elemento);
    }
    public void eliminar(Alumno elemento){
        listaDeAlumnos.remove(elemento);
    }
    public Alumno buscar(int clave){
        Alumno resultado = null;
        for(Alumno auxiliar : listaDeAlumnos){
            if(auxiliar.getClave() == clave){
            resultado = auxiliar;
            break;
            }
        }
        return resultado;
    }
    public Alumno getAlumno( int posicion){
        return listaDeAlumnos.get(posicion);
    }
    public ArrayList<Alumno> getListaDeAlumnos() {
        return listaDeAlumnos;
    }
    public void setListaDeAlumnos(ArrayList<Alumno> listaDeAlumnos) {
        this.listaDeAlumnos = listaDeAlumnos;
    }   
}
