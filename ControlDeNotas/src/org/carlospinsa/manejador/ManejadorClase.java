package org.carlospinsa.manejador;
import java.util.ArrayList;
import org.carlospinsa.bean.Clase;
public class ManejadorClase {
    private ArrayList<Clase> listaDeClase = new ArrayList<Clase>();
    public ManejadorClase() {
    }
    public void agregar(Clase elemento){
        listaDeClase.add(elemento);
    }
    public void eliminar(Clase elemento){
        listaDeClase.remove(elemento);
    }
    public Clase buscar(int codigoClase){
        Clase resultado = null;
        for(Clase auxiliar : listaDeClase){
            if(auxiliar.getCodigoClase()== codigoClase){
                resultado = auxiliar;
            }
        }
        return resultado;
    }
    public Clase getClase(int posicion){
        return listaDeClase.get(posicion);
    }
    public ArrayList<Clase> getListaDeClase() {
        return listaDeClase;
    }
    public void setListaDeClase(ArrayList<Clase> listaDeClase) {
        this.listaDeClase = listaDeClase;
    }  
}
