package org.carlospinsa.manejador;
import java.util.ArrayList;
import org.carlospinsa.bean.Estado;
public class ManejadorEstado {
    private ArrayList <Estado> listaDeEstado = new ArrayList<Estado>();
    public ManejadorEstado(){
    }
    public void agregar(Estado elemento){
        listaDeEstado.add(elemento);
    }
    public void eliminar(Estado elemento){
        listaDeEstado.remove(elemento);
    }
    public Estado buscar(int idEstado){
        Estado resultado = null;
        for(Estado auxiliar : listaDeEstado){
            if(auxiliar.getIdEstado()== idEstado){
                resultado = auxiliar;
                break;
            }
        }
        return resultado;
    }
    public Estado getEstado(int posicion){
        return listaDeEstado.get(posicion);
    }

    public ArrayList<Estado> getListaDeEstado() {
        return listaDeEstado;
    }

    public void setListaDeEstado(ArrayList<Estado> listaDeEstado) {
        this.listaDeEstado = listaDeEstado;
    }
    
}
