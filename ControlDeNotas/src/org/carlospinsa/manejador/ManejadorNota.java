package org.carlospinsa.manejador;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.carlospinsa.bean.Nota;
import org.carlospinsa.bean.Estado;
import org.carlospinsa.bean.Clase;
import org.carlospinsa.db.Conexion;
public class ManejadorNota {
    private ArrayList<Nota> listaDeNotas = new ArrayList <Nota>();
    public ManejadorNota() {
    }
        public ArrayList<Nota> getListaDeNotas(int clave) {
        Map parametros = new HashMap();
        parametros.put("clave",clave );
        ResultSet datos = Conexion.getInstancia().ejecutarProcedimiento("{call sp_ListarNotas(?)}", parametros, true);
          try{
             ArrayList<Nota> lista = new ArrayList<Nota>();
              while(datos.next()){
               lista.add(new Nota(datos.getInt("codigoNota"),datos.getInt("clave"),
                         new Clase(datos.getInt("codigoClase"),datos.getString( "descripcion")),datos.getDouble("primeraUnidad"),datos.getDouble("SegundaUnidad"),datos.getDouble("terceraUnidad"),datos.getDouble("cuartaUnidad"),datos.getDouble("quintaUnidad"),
                         new Estado(datos.getInt("idEstado"),datos.getString("estado"))));
               }
         }catch(SQLException e){
             e.printStackTrace();
        }return listaDeNotas;
    }
    public void agregar(Nota elemento){
        listaDeNotas.add(elemento);
    }
    public void eliminar(Nota elemento){
        listaDeNotas.remove(elemento);
    }
    public Nota buscar(int codigoNota){
        Nota resultado = null;
        for(Nota auxiliar : listaDeNotas){
            if(auxiliar.getCodigoNota()== codigoNota){
            resultado = auxiliar;
            break;
            }
        }
        return resultado;
    }
    public Nota getNota(int posicion){
        return listaDeNotas.get(posicion);
    }

    public void setListaDeNotas(ArrayList<Nota> listaDeNotas) {
        this.listaDeNotas = listaDeNotas;
    }   

    public ArrayList<Nota> getListaDeNotas() {
        return listaDeNotas;
    }
}