package org.carlospinsa.manejador;
import org.carlospinsa.bean.Unidad;
import java.util.ArrayList;
public class ManejadorUnidad {
    private ArrayList<Unidad>listaDeUnidades = new ArrayList<Unidad>();
    public ManejadorUnidad() {
    }
    public void agregar(Unidad elemento){
        listaDeUnidades.add(elemento);
    }
    public void eliminar(Unidad elemento){
        listaDeUnidades.remove(elemento);
    }
    public Unidad buscar(int idUnidad){
        Unidad resultado = null;
        for(Unidad auxiliar : listaDeUnidades){
            if(auxiliar.getIdUnidad() == idUnidad){
                resultado = auxiliar;
                break;
            }    
        }
        return resultado;
    }
    public Unidad getUnidad(int posicion){
        return listaDeUnidades.get(posicion);
    }
    public ArrayList<Unidad> getListaDeUnidades() {
        return listaDeUnidades;
    }
    public void setListaDeUnidades(ArrayList<Unidad> listaDeUnidades) {
        this.listaDeUnidades = listaDeUnidades;
    }
}
