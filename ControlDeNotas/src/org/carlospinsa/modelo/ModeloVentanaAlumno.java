package org.carlospinsa.modelo;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import org.carlospinsa.manejador.ManejadorAlumno;
import org.carlospinsa.bean.Alumno;
public class ModeloVentanaAlumno extends AbstractTableModel {
    private String [] encabezados = {"Clave","Carne","Apellido","nombre"};
    private ArrayList<Alumno> listaDeAlumnos = null;
    private ManejadorAlumno manejador = new ManejadorAlumno();
    public ModeloVentanaAlumno(){
        listaDeAlumnos = manejador.getListaDeAlumnos(); 
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaDeAlumnos.size();
    }
    public Object getValueAt(int fila, int columna){
        String resultado="";
        Alumno elemento = listaDeAlumnos.get(fila);
        switch(columna){
            case 0:
                resultado = String.valueOf(elemento.getClave());
                break;
            case 1:
                resultado = elemento.getCarne();
                break;
            case 2:
                resultado = elemento.getApellido();
                break;
            case 3:
                resultado = elemento.getNombre();
                break;
        }
        return resultado;
    }
}
