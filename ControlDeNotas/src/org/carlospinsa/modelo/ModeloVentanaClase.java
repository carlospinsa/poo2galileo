package org.carlospinsa.modelo;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import org.carlospinsa.manejador.ManejadorClase;
import org.carlospinsa.bean.Clase;
public class ModeloVentanaClase extends AbstractTableModel{
    private String [] encabezados = {"codigoClase","descripcion"};
    private ArrayList <Clase> listaDeClase = null;
    private ManejadorClase manejador = new ManejadorClase();
    public ModeloVentanaClase(){
        listaDeClase = manejador.getListaDeClase();
    }
    public String getColumnName(int columna){
        return encabezados [columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaDeClase.size();
    } 
    public Object getValueAt(int fila,int columna){
        String resultado ="";
        Clase elemento = listaDeClase.get(fila);
        switch (columna){
            case 0:
                resultado = String.valueOf(elemento.getCodigoClase());
                break;
            case 1:
                resultado = elemento.getDescripcion();
                break;
        }
        return resultado;
    }
}
