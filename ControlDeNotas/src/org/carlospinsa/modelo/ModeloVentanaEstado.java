package org.carlospinsa.modelo;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import org.carlospinsa.manejador.ManejadorEstado;
import org.carlospinsa.bean.Estado;
public class ModeloVentanaEstado extends AbstractTableModel{
    private String [] encabezados ={"idEstado","estado"};
    private ArrayList<Estado> listaDeEstado = null;
    private ManejadorEstado manejador = new ManejadorEstado(); 
    public ModeloVentanaEstado(){
        listaDeEstado = manejador.getListaDeEstado();
    }
    public String getColumnName(int columna){
        return encabezados [columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaDeEstado.size();
    }
    public Object getValueAt(int fila,int columna){
        String resultado="";
        Estado elemento = listaDeEstado.get(fila);
        switch (columna){
            case 0:
                resultado = String.valueOf(elemento.getIdEstado());
                break;
            case 1:
                resultado = elemento.getEstado();
                break;
        }
            return resultado;
    }
}
