package org.carlospinsa.modelo;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import org.carlospinsa.manejador.ManejadorNota;
import org.carlospinsa.bean.Nota;
public class ModeloVentanaNota  extends AbstractTableModel{
    private String[] encabezados ={"codigo nota","clave","Materia","primera Unidad","segunda Unidad","tercera Unidad","cuarta Unidad","quinta Unidad","descripcion"};
    private ArrayList <Nota> listaDeNotas = null;
    private ManejadorNota manejador = new ManejadorNota();
    public ModeloVentanaNota(){
        listaDeNotas = manejador.getListaDeNotas();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaDeNotas.size();
    }
    public Object getValueAt(int fila, int columna){
        String resultado="";
          Nota elemento = listaDeNotas.get(fila);
          switch (columna){
              case 0:
                  resultado = String.valueOf(elemento.getCodigoNota());
                  break;
              case 1:
                  resultado = String.valueOf(elemento.getClave());
                  break;
              case 2:
                  resultado = String.valueOf(elemento.getDescripcion());
                  break;
              case 3:
                  resultado = String.valueOf(elemento.getPrimeraUnidad());
                  break;
              case 4:
                  resultado = String.valueOf(elemento.getSegundaUnidad());
                  break;
              case 5:
                  resultado = String.valueOf(elemento.getTerceraUnidad());
                  break;
              case 6:
                  resultado = String.valueOf(elemento.getCuartaUnidad());
                  break;
              case 7:
                  resultado = String.valueOf(elemento.getQuintaUnidad());
                  break;
              case 8:
                  resultado = String.valueOf(elemento.getIdEstado());
                  break;
          }
          return resultado;
    }
    
}
