package org.carlospinsa.modelo;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import org.carlospinsa.manejador.ManejadorUnidad;
import org.carlospinsa.bean.Unidad;
public class ModeloVentanaUnidad extends AbstractTableModel{
    private String [] encabezados = {"idUnidad","unidad"};
    private ArrayList <Unidad> listaDeUnidades = null;
    private ManejadorUnidad manejador = new ManejadorUnidad();
    public ModeloVentanaUnidad(){
        listaDeUnidades = manejador.getListaDeUnidades();
    }
    public String getColumnName(int columna){
        return encabezados [columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaDeUnidades.size();
    }
    public Object getValueAt(int fila, int columna){
        String resultado = "";
        Unidad elemento = listaDeUnidades.get(fila);
            switch (columna){
                case 0:
                    resultado = String.valueOf(elemento.getIdUnidad());
                    break;
                case 1:
                    resultado = elemento.getUnidad();
                    break;
            }
        return resultado;
    }
}
